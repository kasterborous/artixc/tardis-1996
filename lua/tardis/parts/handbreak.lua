local PART={}
PART.ID = "mcghandbreak"
PART.Name = "1996 handbreak"
PART.Text = "Dematerialisation Circuit"
PART.Model = "models/artixc/mcgann/handbreak.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 2

PART.Sound = "drmatt/tardis/default/control_handbrake.wav"

TARDIS:AddPart(PART,e)
