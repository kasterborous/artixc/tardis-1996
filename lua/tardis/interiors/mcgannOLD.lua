--[[ OLD SYSTEM CODE DO NOT USE!

local T={}
T.Base="base"
T.Name="1996 McGann"
T.ID="mcganna"
T.Interior={
	Model="models/artixc/mcgann/basicroom.mdl",
	LightOverride = {
		basebrightness = 0.01,
		nopowerbrightness = 0.003
	},
	Portal={
	        pos=Vector(153.6,501,71.2),
	        ang=Angle(0,-90,0),
	        width=45,
	        height=90
	},
	Fallback={
		pos=Vector(0,-206,0.5),
		ang=Angle(0,90,0),
	},
	ExitDistance=600,
	IdleSound={
		{
			path="artixc/mcgann/interioramb.wav",
			volume=0.2
		}
	},
	Light={
	      color=Color(0,162,255),
	      pos=Vector(0,0,40),
	      brightness=1
	
	},
	Lights={
		{
		color=Color(0,102,255),
		pos=Vector(100,100,230),
		brightness=5
		},
		{
		color=Color(255,255,255),
		pos=Vector(160,450,90),
		brightness=1
		}
  },
	Parts={
		console={
			model="models/artixc/mcgann/consolebasics.mdl"
		},
                door={
                        model="models/artixc/mcgann/extdoor.mdl",posoffset=Vector(23.5,0,-49),angoffset=Angle(0,184,0)
                },
		consolefloor=true,
		pillar1=true,
		pillar2=true,
		consolegurt=true,
		bookshelf96=true,
		goldem=true,
		cabwall=true,
		mcgannrot=true,
		mcganntoprol=true,
		consometal=true,
		mcgannmidrol=true,
		mcgannbot1rol=true,
		mcgannbot2rol=true,
		mcgannbot3rol=true,
		mcgannbot4rol=true,
		mcgannbot5rol=true,
		mcgannbot6rol=true,
		glowcon=true,
		mcghandbreak=true,
		mcgannwind=true,
	}
}
T.Exterior={
	Model="models/artixc/mcgann/exterior.mdl",
	Mass=2000,
	Portal={
		pos=Vector(23.5,0,48),
		ang=Angle(0,0,0),
		width=45,
		height=92
	},
	Fallback={
		pos=Vector(35,0,5),
		ang=Angle(0,0,0)
	},
	Light={
		enabled=true,
		pos=Vector(0,0,112),
		color=Color(80,80,255)
	},
	Sounds={
		Teleport={
			demat="artixc/mcgann/demat.wav",
			mat="artixc/mcgann/mat.wav"
		},
		Lock="doctorwho1200/baker/lock.wav", --TEMP 
		Door={
			enabled=true,
			open="doctorwho1200/baker/doorext_open.wav", --TEMP 
			close="doctorwho1200/baker/doorext_close.wav" --TEMP 
		},
		FlightLoop="artixc/mcgann/flight_loop.wav"
	},
	Parts={
                door={
                        model="models/artixc/mcgann/extdoor.mdl",
						posoffset=Vector(-23.3,0,-49),
						angoffset=Angle(0,0,0),
                },
		vortex={
			model="models/doctorwho1200/baker/1974timevortex.mdl",--TEMP 
			pos=Vector(0,0,50),
			ang=Angle(0,0,0),
			scale=10
		}
	},
	Teleport = {
		SequenceSpeed = 0.60,
		SequenceSpeedFast = 0.935,
		DematSequence = {
			255,
			200,
			150,
			100,
			70,
			50,
			20,
			0
		},
		MatSequence = {
			0,
			20,
			50,
			100,
			150,
			180,
			255
		}
	}
}

TARDIS:AddInterior(T)
]]